<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210221094108 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE shop (id BIGSERIAL NOT NULL, name VARCHAR(255) NOT NULL, alias VARCHAR(32) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AC6A4CA2E16C6B94 ON shop (alias)');
        $this->addSql('CREATE TABLE shop_hit (id BIGSERIAL NOT NULL, shop_id BIGINT DEFAULT NULL, type VARCHAR(255) NOT NULL, ip VARCHAR(255) NOT NULL, browser VARCHAR(255) NOT NULL, device VARCHAR(255) NOT NULL, referrer VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C929F67F4D16C4DD ON shop_hit (shop_id)');
        $this->addSql('ALTER TABLE shop_hit ADD CONSTRAINT FK_C929F67F4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE shop_hit DROP CONSTRAINT FK_C929F67F4D16C4DD');
        $this->addSql('DROP TABLE shop');
        $this->addSql('DROP TABLE shop_hit');
    }
}
