<?php

namespace App\Normalizer;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * Class ViolationErrorNormalizer
 * @package App\Validator
 */
class ViolationErrorNormalizer
{
    /**
     * @param ConstraintViolationListInterface $violations
     * @return array
     */
    public function normalize(ConstraintViolationListInterface $violations)
    {
        $errors = [];

        /** @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            $propertyPath = $violation->getPropertyPath();
            if ('[' !== substr($propertyPath, 0, 1)) {
                $propertyPath = preg_replace("/([^\[\]]+)(.*)/", "[$1]$2", $propertyPath);
            }

            try {
                $errorMessage = $violation->getCode() ? $violation->getConstraint()::getErrorName($violation->getCode()) : null;
            } catch (InvalidArgumentException $e) {
                $errorMessage = null;
            }

             $error = [
                'message' => $violation->getMessage(),
                'code' => $errorMessage,
                'payload' => $violation->getConstraint()->payload
            ];

            if ($propertyPath) {
                $errors[$propertyPath] = $error;
                continue;
            }

            $errors[] = $error;
        }
        $errorText = (count($errors) > 1) ? 'Validation failed' : $violation->getMessage();

        return [
            'message' => $errorText,
            'errors' => $errors,
        ];
    }
}
