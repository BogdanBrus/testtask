<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use Detection\MobileDetect;

/**
 * Class RequestUserInfo
 * @package App\Services
 */
class RequestUserInfo
{
    // Devices
    const DESKTOP_DEVICE = 'desktop';
    const MOBILE_DEVICE = 'mobile';
    const TABLET_DEVICE = 'tablet';

    // Fields in Full Info
    const IP_FILED = 'ip';
    const REFERER_FILED = 'referrer';
    const DEVICE_FILED = 'device';
    const BROWSER_FILED = 'browser';


    /** @var RequestStack  */
    private $request;

    /**
     * @var MobileDetect
     */
    private $mobileDetect;

    /**
     * RequestUserInfo constructor.
     * @param RequestStack $request
     * @param MobileDetect $mobileDetect
     */
    public function __construct(RequestStack $request, MobileDetect $mobileDetect)
    {
        $this->request = $request->getCurrentRequest();
        $this->mobileDetect = $mobileDetect;
    }

    /**
     * @return array
     */
    public function getFullInfo()
    {
        return [
            self::IP_FILED => $this->getIp(),
            self::REFERER_FILED => $this->getReferer(),
            self::DEVICE_FILED => $this->getDevice(),
            self::BROWSER_FILED => $this->getBrowser(),
        ];
    }

    /**
     * @return string|null
     */
    public function getIp()
    {
        return $this->request->getClientIp();
    }

    /**
     * @return string|null
     */
    public function getReferer()
    {
        return $this->request->server->get('HTTP_REFERER');
    }

    /**
     * @return string|null
     */
    public function getBrowser()
    {
        return $this->getUserAgent();
    }

    /**
     * @return string|null
     */
    public function getDevice()
    {
        $this->mobileDetect
            ->setUserAgent($this->getUserAgent());

        $device = self::DESKTOP_DEVICE;
        if ($this->mobileDetect->isTablet()) {
            $device = self::TABLET_DEVICE;
        } elseif ($this->mobileDetect->isMobile()) {
            $device = self::MOBILE_DEVICE;
        }

        return $device;
    }

    /**
     * @return string|null
     */
    public function getUserAgent()
    {
        return $this->request->headers->get('USER_AGENT');
    }
}
