<?php

namespace App\Repository;

use App\Entity\Shop;
use Doctrine\ORM\EntityRepository;

/**
 * Class ShopRepository
 * @package App\Repository
 */
class ShopRepository extends EntityRepository
{
    /**
     * @param $alias
     * @return Shop|object|null
     */
    public function findByAlias($alias)
    {
        return $this->findOneBy([
            'alias' => $alias,
        ]);
    }

    /**
     * @param array $shopData
     * @return Shop
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(array $shopData): Shop
    {
        $shop = new Shop();
        $shop
            ->setAlias($shopData['alias'])
            ->setName($shopData['name']);

        $this->_em->persist($shop);

        return $shop;
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getListQuery()
    {
       return $this->createQueryBuilder('shop');
    }
}
