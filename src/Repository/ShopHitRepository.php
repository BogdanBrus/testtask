<?php

namespace App\Repository;

use App\Entity\Shop;
use App\Entity\ShopHit;
use App\Services\RequestUserInfo;
use Doctrine\ORM\EntityRepository;

/**
 * Class ShopHitRepository
 * @package App\Repository
 */
class ShopHitRepository extends EntityRepository
{
    /**
     * @param Shop $shop
     * @param string $type
     * @param array $userRequestInfo
     * @return ShopHit
     * @throws \Doctrine\ORM\ORMException
     */
    public function create(Shop $shop, string $type, array $userRequestInfo): ShopHit
    {
        $shopHit = new ShopHit();
        $shopHit
            ->setType($type)
            ->setShop($shop)
            ->setBrowser($userRequestInfo[RequestUserInfo::BROWSER_FILED])
            ->setDevice($userRequestInfo[RequestUserInfo::DEVICE_FILED])
            ->setIp($userRequestInfo[RequestUserInfo::IP_FILED])
            ->setReferrer($userRequestInfo[RequestUserInfo::REFERER_FILED]);

        $this->_em->persist($shopHit);

        return $shopHit;
    }

    /**
     * @param Shop $shop
     * @param string $type
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCount(Shop $shop, string $type): int
    {
        $query = $this->createQueryBuilder('shopHits')
            ->select('COUNT(shopHits.id)')
            ->where('shopHits.shop = (:shop)')
            ->setParameter('shop', $shop);

        if ('' !== $type) {
            $query->andWhere('shopHits.type = (:type)')
                ->setParameter('type', $type);
        }

        return $query->getQuery()
            ->getSingleScalarResult();
    }
}
