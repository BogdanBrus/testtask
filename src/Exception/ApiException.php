<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ApiException
 * @package App\Exception
 */
class ApiException extends HttpException
{
    const INTERNAL_ERROR = 'INTERNAL_ERROR';
    const VALIDATION_FAILED = 'VALIDATION_FAILED';

    /**
     * @var string
     */
    private $errorCode;

    /**
     * @var array
     */
    private $errors = [];

    /**
     * ApiException constructor.
     *
     * @param string|null $message
     * @param string      $errorCode
     * @param int         $statusCode
     * @param array       $errors
     */
    public function __construct(string $message = null, string $errorCode = self::INTERNAL_ERROR, $statusCode = 500, array $errors = [])
    {
        $this->errorCode = $errorCode;
        $this->errors = $errors;

        parent::__construct($statusCode, $message);
    }

    /**
     * @return string
     */
    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
