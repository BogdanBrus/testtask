<?php

namespace App\Controller;

use App\Entity\Shop;
use App\Entity\ShopHit;
use App\Exception\ApiException;
use App\Services\RequestUserInfo;
use App\Normalizer\ViolationErrorNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/shop_hit")
 *
 * Class ShopHitController
 * @package App\Controller
 */
class ShopHitController extends AbstractBaseController
{
    /** @var RequestUserInfo  */
    private $requestUserInfoService;

    /**
     * ShopHitController constructor.
     * @param RequestUserInfo $requestUserInfoService
     */
    public function __construct(RequestUserInfo $requestUserInfoService)
    {
        $this->requestUserInfoService = $requestUserInfoService;
    }

    /**
     * @Route("/{alias}/count/{type<[a-z_]+>}", methods={"GET"}, name="shop_hit.count", defaults={"type"=""})
     * @param Shop $shop
     * @param string $type
     * @param ValidatorInterface $validator
     * @param ViolationErrorNormalizer $violationErrorNormalizer
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getCount(Shop $shop, string $type, ValidatorInterface $validator, ViolationErrorNormalizer $violationErrorNormalizer)
    {
        if ('' !== $type) {
            $this->validateTypeOrThrowBadRequest($type, $validator, $violationErrorNormalizer);
        }

        $shopHitRepository = $this->getDoctrine()->getRepository(ShopHit::class);
        $count = $shopHitRepository->getCount($shop, $type);;

        return $this->json([
            'count' => $count,
        ]);
    }

    /**
     * @Route("/{alias}/create/{type<[a-z_]+>}", methods={"POST"}, name="shop_hit.create")
     * @param Shop $shop
     * @param string $type
     * @param ValidatorInterface $validator
     * @param ViolationErrorNormalizer $violationErrorNormalizer
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Shop $shop, string $type, ValidatorInterface $validator, ViolationErrorNormalizer $violationErrorNormalizer)
    {
        $this->validateTypeOrThrowBadRequest($type, $validator, $violationErrorNormalizer);

        $shopHitRepository = $this->getDoctrine()->getRepository(ShopHit::class);
        $shopHitRepository->create($shop, $type, $this->requestUserInfoService->getFullInfo());

        $this->getEntityManager()->flush();

        return $this->json([
            'message' => 'ShopHit has been created successfully!'
        ]);
    }

    /**
     * @Route("/list_types", methods={"GET"}, name="shop_hit.list_types")
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function listOfTypes()
    {
        return $this->json(ShopHit::getAvailableTypes());
    }

    /**
     * @param string $type
     * @param ValidatorInterface $validator
     * @param ViolationErrorNormalizer $violationErrorNormalizer
     */
    private function validateTypeOrThrowBadRequest(string $type, ValidatorInterface $validator, ViolationErrorNormalizer $violationErrorNormalizer)
    {
        $listTypesUrl = $this->generateUrl('shop_hit.list_types');
        $constraints = new Constraints\Choice([
            'value' => ShopHit::getAvailableTypes(),
            'message' => "Type '$type'' is not available in ShopHit. Please check list of available types: $listTypesUrl"
        ]);

        $violations = $validator->validate($type, $constraints);

        if ($violations->count()) {
            $errorsNormalized = $violationErrorNormalizer->normalize($violations);
            throw new ApiException(
                $errorsNormalized['message'],
                ApiException::VALIDATION_FAILED,
                Response::HTTP_BAD_REQUEST,
                $errorsNormalized['errors']
            );
        }
    }
}
