<?php

namespace App\Controller;

use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractBaseController - for custom logic
 * @package App\Controller
 */
class AbstractBaseController extends AbstractController
{
    const MAX_RESULTS_PER_PAGE_DEFAULT = 10;

    /**
     * @return \Doctrine\Persistence\ObjectManager
     */
    protected function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * Example Usage: ?page=1&perPage=2
     *
     * @param Request $request
     * @param QueryBuilder $queryBuilder
     * @param int $perPage
     * @return array
     */
    protected function paginateByRequest(Request $request, QueryBuilder $queryBuilder, int $perPage = null)
    {
        $page = $request->get('page', 1);

        if (null === $perPage) {
            $perPageFromRequest = $request->get('perPage');
            $perPage = $perPageFromRequest ?? self::MAX_RESULTS_PER_PAGE_DEFAULT;
        }

        $first = $page * $perPage - $perPage;

        return $queryBuilder
            ->setFirstResult($first)
            ->setMaxResults($perPage)
            ->getQuery()
            ->getArrayResult();
    }
}
