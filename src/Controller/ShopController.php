<?php

namespace App\Controller;

use App\Entity\Shop;
use App\Exception\ApiException;
use App\Normalizer\ViolationErrorNormalizer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/shop")
 *
 * Class ShopController
 * @package App\Controller
 */
class ShopController extends AbstractBaseController
{
    /**
     * @Route("/list", methods={"GET"}, name="shop.list")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getList(Request $request)
    {
        $shopRepository = $this->getDoctrine()->getRepository(Shop::class);
        $shopListQuery = $shopRepository->getListQuery();

        return $this->json($this->paginateByRequest($request, $shopListQuery));
    }

    /**
     * @Route("/create", methods={"POST"}, name="shop.create")
     * @param Request $request
     * @param ValidatorInterface $validator
     * @param ViolationErrorNormalizer $violationErrorNormalizer
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function create(Request $request, ValidatorInterface $validator, ViolationErrorNormalizer $violationErrorNormalizer)
    {
        $inputProperties = $request->request->all();
        $alias = $request->request->get('alias');

        // Validate input
        $constraints = new Constraints\Collection([
            'name' => new Constraints\Required([
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
            ]),
            'alias' => new Constraints\Required([
                new Constraints\NotBlank(),
                new Constraints\Type('string'),
            ]),
        ]);

        $violations = $validator->validate($inputProperties, $constraints);

        if ($violations->count()) {
            $errorsNormalized = $violationErrorNormalizer->normalize($violations);
            throw new ApiException(
                $errorsNormalized['message'],
                ApiException::VALIDATION_FAILED,
                Response::HTTP_BAD_REQUEST,
                $errorsNormalized['errors']
            );
        }

        // Check existing by alias
        $shopRepository = $this->getDoctrine()->getRepository(Shop::class);
        $existingShop = $shopRepository->findByAlias($alias);

        if (null !== $existingShop) {
            throw new ApiException(
                "Shop with alias: '$alias' already exists.",
                ApiException::VALIDATION_FAILED,
                Response::HTTP_BAD_REQUEST
            );
        }

        /** @var Shop $shop */
        $shop = $shopRepository->create($inputProperties);
        $this->getEntityManager()->flush();

        return $this->json([
            'message' => 'Shop has been created successfully!',
            'id' => $shop->getId(),
        ]);
    }
}
