<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ShopHit
 * @package App\Entity
 *
 * @ORM\Entity(repositoryClass="App\Repository\ShopHitRepository")
 *
 * TODO: think about moving User Info to separate table ShopHitUser. Depends on how it's going to be used: always together or separately
 * TODO: Probably create trait with RequestUserInfo fields -> if it will be using in many place in the same format
 */
class ShopHit
{
    const VIEW_PAGE_TYPE = 'view_page';
    const CLICK_BANNER_TYPE = 'click_banner';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $browser;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $device;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $referrer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="shopHits")
     */
    private $shop;

    /**
     * ShopHit constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return array
     */
    public static function getAvailableTypes()
    {
        return [
            self::VIEW_PAGE_TYPE,
            self::CLICK_BANNER_TYPE,
        ];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param Shop $shop
     * @return $this
     */
    public function setShop(Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * @return Shop
     */
    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    /**
     * @return string
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return $this
     */
    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return string
     */
    public function getBrowser(): ?string
    {
        return $this->browser;
    }

    /**
     * @param string $browser
     * @return $this
     */
    public function setBrowser(string $browser): self
    {
        $this->browser = $browser;

        return $this;
    }

    /**
     * @return string
     */
    public function getDevice(): ?string
    {
        return $this->device;
    }

    /**
     * @param string $device
     * @return $this
     */
    public function setDevice(string $device): self
    {
        $this->device = $device;

        return $this;
    }

    /**
     * @return string
     */
    public function getReferrer(): ?string
    {
        return $this->referrer;
    }

    /**
     * @param string|null $referrer
     * @return $this
     */
    public function setReferrer($referrer): self
    {
        $this->referrer = $referrer;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
