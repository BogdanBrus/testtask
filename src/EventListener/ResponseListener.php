<?php

namespace App\EventListener;

use App\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

/**
 * Class ResponseListener
 * @package App\EventListener
 */
class ResponseListener
{
    /**
     * Handle App Exceptions
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $errors = [
            'message' => $exception->getMessage(),
            'code' => ApiException::INTERNAL_ERROR,
            'errors' => []
        ];
        $response = new JsonResponse();

        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());

            if ($exception instanceof ApiException) {
                $errors['errorCode'] = $exception->getErrorCode();
                $errors['statusCode'] = $exception->getStatusCode();
                $errors['errors'] = $exception->getErrors();
            }
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $response->setData($errors);
        $event->setResponse($response);
    }
}
