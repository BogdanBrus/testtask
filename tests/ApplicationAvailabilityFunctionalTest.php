<?php

namespace App\Test;

use App\Entity\ShopHit;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApplicationAvailabilityFunctionalTest
 * @package App\Test
 */
class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProviderIsSuccessful
     * @param string $method
     * @param string $url
     * @param array $params
     */
    public function testRouteStatus(string $method, string $url, array $params = [])
    {
        $client = self::createClient();
        $client->request($method, $url, $params);

        $this->assertResponseIsSuccessful($client->getRequest()->getContent());

    }

    /**
     * Routes with response 200
     * @return \Generator
     */
    public function urlProviderIsSuccessful()
    {
        $shopAlias = 'shop1';
        $shopHitType = ShopHit::VIEW_PAGE_TYPE;

        // ShopController
        yield [
            'method' => 'POST',
            'url' => "/shop/create",
            'params' => [
                'name' => $shopAlias,
                'alias' => $shopAlias
            ]
        ];
        yield [
            'method' => 'GET',
            'url' => '/shop/list?page=1&perPage=5',
        ];

        // ShopHitController
        yield [
            'method' => 'POST',
            'url' => "/shop_hit/$shopAlias/create/$shopHitType",
        ];
        yield [
            'method' => 'GET',
            'url' => "/shop_hit/$shopAlias/count/$shopHitType",
        ];
        yield [
            'method' => 'GET',
            'url' => "/shop_hit/$shopAlias/count",
        ];
        yield [
            'method' => 'GET',
            'url' => '/shop_hit/list_types',
        ];
    }
}
