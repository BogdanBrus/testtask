<?php

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__).'/vendor/autoload.php';

if (file_exists(dirname(__DIR__).'/config/bootstrap.php')) {
    require dirname(__DIR__).'/config/bootstrap.php';
} elseif (method_exists(Dotenv::class, 'bootEnv')) {
    (new Dotenv())->bootEnv(dirname(__DIR__).'/.env');
}


// drop schema for _test db
passthru(sprintf('%s "%s/../bin/console" doctrine:schema:drop --full-database --no-interaction --force --env=test > /dev/null', PHP_BINARY, __DIR__));
// Test migrations UP
passthru(sprintf('%s "%s/../bin/console" doctrine:migrations:migrate --no-interaction --env=test > /dev/null', PHP_BINARY, __DIR__));


